package articles;

import java.awt.*;
import java.awt.event.*;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import javax.swing.*;

public class GUI extends JFrame implements Runnable, ActionListener {
	private int percent;
	private JButton createButton = new JButton("������� ����");
	private JCheckBox checkBox = new JCheckBox("������������ ����", false);
	private JTextArea urlTextArea = new JTextArea();
	private JTextArea pageCountTextArea = new JTextArea();
	private JTextArea titleTextArea = new JTextArea();
	private JTextArea nameTextArea = new JTextArea();
	private JTextField chooser = new JTextField();
	private JProgressBar progressBar;
	private JLabel chooserFileLabel = new JLabel("�������� ����:");
	private JLabel urlLabel = new JLabel("������ url:");
	private JLabel pageCountLabel = new JLabel("���������� �������:");
	private JLabel titleLabel = new JLabel("������ \"�����\":");
	private JLabel nameLabel = new JLabel("������ ��������:");
	private JLabel resultLabel = new JLabel("");
	private DefaultBoundedRangeModel model = new DefaultBoundedRangeModel(0, 0, 0, 100);

	public GUI() {
		super("������� ������");
		this.setBounds(50, 50, 1200, 800);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container container = this.getContentPane();
		container.setLayout(new GridLayout(4, 1, 10, 10));
		Panel p1 = new Panel(new GridLayout(4, 1, 10, 10));
		Panel p2 = new Panel(new GridLayout(1, 1, 2, 2));
		Panel p3 = new Panel(new GridLayout(1, 3, 2, 2));
		Panel p4 = new Panel(new GridLayout(3, 1, 2, 2));
		Panel p31 = new Panel(new GridLayout(1, 1, 2, 2));

		Panel p32 = new Panel(new GridLayout(1, 1, 2, 2));
		Panel p33 = new Panel(new GridLayout(1, 1, 2, 2));

		progressBar = new JProgressBar(model);
		progressBar.setBounds(50, 170, 200, 20);
		progressBar.setStringPainted(true);

		p1.add(chooserFileLabel);
		p1.add(chooser);
		p1.add(checkBox);
		p1.add(urlLabel);

		p2.add(urlTextArea);

		p31.add(pageCountLabel);
		p31.add(pageCountTextArea);

		p32.add(titleLabel);
		p32.add(titleTextArea);

		p33.add(nameLabel);
		p33.add(nameTextArea);

		p3.add(p31);
		p3.add(p32);
		p3.add(p33);

		p4.add(createButton);
		p4.add(progressBar);
		p4.add(resultLabel);

		container.add(p1);
		container.add(p2);
		container.add(p3);
		container.add(p4);
		createButton.addActionListener(this);
	}

	@Override
	public void run() {
		model.setValue(percent);
	}

	public void actionPerformed(ActionEvent e) {
		String file = chooser.getText();
		progressBar.setValue(0);
		resultLabel.setText("");
		percent=0;

		String urlArrayString[] = getArray(urlTextArea.getText().split("\n"));
		String pageCountArrayString[] = getArray(pageCountTextArea.getText().split("\n"));
		String nameArrayString[] = getArray(nameTextArea.getText().split("\n"));
		String titleArrayString[] = getArray(titleTextArea.getText().split("\n"));

		int[] pageCountArray = new int[pageCountArrayString.length];
		for (int i = 0; i < pageCountArray.length; i++) {
			pageCountArray[i] = Integer.parseInt(pageCountArrayString[i]);
		}

		boolean isNewFile = checkBox.isSelected();
		int count = 0;
		for (int i = 0; i < pageCountArray.length; i++) {
			count += pageCountArray[i];
		}
		progressBar.setMaximum(count);
		Calculator c = new Calculator(file, urlArrayString, pageCountArray, nameArrayString, titleArrayString,
				isNewFile);
		c.start();
	}

	private String[] getArray(String[] strings) {
		int s = 0;
		for (int i = 0; i < strings.length; i++) {
			if (!strings[i].isEmpty()) {
				s++;
			}
		}
		String[] newArray = new String[s];
		s = 0;
		for (int i = 0; i < strings.length; i++) {
			if (!strings[i].isEmpty()) {
				newArray[s] = strings[i];
				s++;
			}
		}
		return newArray;
	}

	class Calculator extends Thread {
		String fileString;
		String[] url;
		int[] page;
		String[] name;
		String[] title;
		boolean isNew;

		public Calculator(String fileString, String[] url, int[] page, String[] name, String[] title, boolean isNew) {
			super();
			this.fileString = fileString;
			this.url = url;
			this.page = page;
			this.name = name;
			this.title = title;
			this.isNew = isNew;
		}

		public void run() {
			if ((url.length == page.length) & (url.length == name.length) & (url.length == title.length)
					& (url.length != 0)) {
				if (isNew) {
					File file2 = new File(fileString);
					if (file2.exists()) {
						file2.delete();
					}
				}
				for (int i = 0; i < name.length; i++) {
					try {
						getArticles(fileString, page[i], url[i], title[i], name[i]);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				resultLabel.setText("���� ������");
			}
		}

		public void createFile(String pathname) throws Exception {
			File file = new File(pathname);
			file.createNewFile();
		}

		public void saveData(String content, String pathname) throws Exception {
			File file = new File(pathname);
			BufferedWriter out = null;
			try {
				out = new BufferedWriter(new FileWriter(file, true));
				out.write(content + "\n");

			} finally {
				out.close();
			}
		}

		public void getArticles(String file, int pageCount, String urlString, String title, String name)
				throws Exception {
			for (int i = 1; i <= pageCount; i++) {
				URL url = new URL(String.format(urlString, i));
				BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream(), "utf-8"));
				String inputLine = in.readLine();
				while ((inputLine = in.readLine()) != null) {
					if (inputLine.indexOf(title) != -1) {
						String[] strings = inputLine.split("<.*?>");
						int number = getNumber(strings);
						if (number != -1) {
							String resulteString = strings[number];
							saveData(name + "," + resulteString.replace(",", " "), file);
						}
					}
				}
				in.close();
				percent++;
				SwingUtilities.invokeLater(GUI.this);
			}
		}

		private int getNumber(String[] strings) {
			for (int i = 0; i < strings.length; i++) {
				if (!strings[i].trim().isEmpty()) {
					return i;
				}
			}
			return -1;
		}

	}
}
