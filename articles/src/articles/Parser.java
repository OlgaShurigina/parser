package articles;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.URL;

public class Parser {
	public static void getArticles(String file, int pageCount, String urlString, String title, String name)
			throws Exception {
		for (int i = 1; i <= pageCount; i++) {
			URL url = new URL(String.format(urlString));
			BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream(), "utf-8"));
			String inputLine = in.readLine();
			while ((inputLine = in.readLine()) != null) {
				if (inputLine.indexOf(title) != -1) {
					String[] strings = inputLine.split("<.*?>");
					int number = getNumber(strings);
					if (number != -1) {
						String resulteString = strings[number];
						saveData(name + "," + resulteString.replace(",", " "), file);
					}
				}
			}
			in.close();
		}
	}

	private static int getNumber(String[] strings) {
		for (int i = 0; i < strings.length; i++) {
			if (!strings[i].trim().isEmpty()) {
				return i;
			}
		}
		return -1;
	}

	private static void createFile(String pathname) throws Exception {
		File file = new File(pathname);
		file.createNewFile();
	}

	private static void saveData(String content, String pathname) throws Exception {
		File file = new File(pathname);
		if (!file.exists()) {
			createFile(pathname);
		}
		BufferedWriter out = null;
		try {
			out = new BufferedWriter(new FileWriter(file, true));
			out.write(content + "\n");

		} finally {
			out.close();
		}
	}
}
